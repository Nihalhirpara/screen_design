import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class InsertUserPage extends StatefulWidget {


  InsertUserPage(this.map);

  Map? map;

  @override
  State<InsertUserPage> createState() => _InsertUserPageState();
}

class _InsertUserPageState extends State<InsertUserPage> {
  var formKey = GlobalKey<FormState>();

  var nameController = TextEditingController();

  var priceController = TextEditingController();



  @override
  void initState() {
    super.initState();
    nameController.text = widget.map==null?'':widget.map!['name'];
    priceController.text = widget.map==null?'':widget.map!['price'];
  }


  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(key: formKey,
        child: Column(
          children: [
            TextFormField(decoration: InputDecoration(hintText: 'Enter Name'),validator: (value){
              if(value!=null && value.isEmpty){
                return "Enter valid Name";
              }
            },
            controller: nameController,
            ),
            TextFormField(decoration: InputDecoration(hintText: 'Enter Price'),validator: (value){
              if(value!=null && value.isEmpty){
                return "Enter valid Price";
              }
            },
            controller: priceController,
            ),
            TextButton(onPressed: (){
              if(formKey.currentState!.validate()){
                if(widget.map == null){
                  insertUser().then((value) => Navigator.of(context).pop(true));
                }else{
                  updateUser(widget.map!['id']).then((value) => Navigator.of(context).pop(true));
              }
              }
            }, child: Text('Submit'))
          ],
        ),
      ),
    );
  }

  Future<void> updateUser(id) async {
    Map map = {};
    map['name'] = nameController.text;
    map['price'] = priceController.text;
    var response1 = await http.put(Uri.parse('https://6315f93d5b85ba9b11ec688e.mockapi.io/shopping/$id'),
        body: map);
    // print(response.body.toString());
    print('response1');
  }

  Future<void> insertUser() async {
    Map map = {};
    map['name'] = nameController.text;
    map['price'] = priceController.text;
    var response1 = await http.post(Uri.parse('https://6315f93d5b85ba9b11ec688e.mockapi.io/shopping'),
        body: map);
    // print(response.body.toString());
    print('response1');
  }
}
