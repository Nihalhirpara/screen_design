import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:screen_design/Api/insert_user.dart';

class LoadLocaljson extends StatefulWidget {
  @override
  State<LoadLocaljson> createState() => _LoadLocaljsonState();
}

class _LoadLocaljsonState extends State<LoadLocaljson> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(actions: [
        InkWell(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(
              Icons.add,
              color: Colors.white,
              size: 24,
            ),
          ),
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (context){
              return InsertUserPage(null);
            },)).then((value) {
              if(value == true){
                setState(() {

                });
              }
            });
          },
        )],),
      body: FutureBuilder<dynamic>(
        builder: (context, snapshot) {
          if (snapshot.hasData) {
          // List<dynamic> datas = jsonDecode(snapshot.data!.body.toString());
          // datas.reversed;
            return ListView.builder(
                itemCount: snapshot.data!.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(builder: (context){
                        return InsertUserPage(snapshot.data![index]);
                      },)).then((value) {
                        if(value == true){
                          setState(() {
                          });
                        }
                      });
                    } ,
                    child: Card(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: [
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    snapshot.data![index]
                                            ['name']
                                        .toString()),
                                ]
                              ),
                            ),
                            Icon(Icons.chevron_right)
                          ],
                        ),
                      ),
                    ),
                  );
                });

            //   var jsonData = json.decode(snapshot.data.toString());
            // return Center(
            //   child: Text(
            //     jsonData[2]['FacultyName'].toString(),
            //   ),
            // );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
        future: getDataFromWebServer(),
      ),
    );
  }


}
Future<dynamic> getDataFromWebServer() async {
  var response = await http
      .get(Uri.parse('https://6315f93d5b85ba9b11ec688e.mockapi.io/shopping'));
  dynamic map = jsonDecode(response.body.toString());

  return map;
}