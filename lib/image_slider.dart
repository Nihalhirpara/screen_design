import 'package:flutter/material.dart';
import 'package:flutter_image_slideshow/flutter_image_slideshow.dart';

class ImageSlider extends StatelessWidget {
  var img1;
  var img2;
  var img3;
  ImageSlider({super.key,this.img1,this.img2,this.img3});

  @override
  Widget build(BuildContext context) {
    return ImageSlideshow(

      height: 250,
      indicatorBackgroundColor: Colors.white,
      indicatorColor: Colors.redAccent,
      indicatorRadius: 4,
      children: [
        Padding(padding: EdgeInsets.all(15),
        child: Image.asset(img1),
        ),
        Padding(padding: EdgeInsets.all(15),
        child: Image.asset(img2),
        ),
        Padding(padding: EdgeInsets.all(15),
        child: Image.asset(img3),
        ),
      ],
    );
  }
}