import 'package:flutter/material.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:line_icons/line_icons.dart';
import 'package:screen_design/cart_screen.dart';
import 'package:screen_design/home_screen.dart';

class Screens extends StatefulWidget {
  @override
  State<Screens> createState() => _ScreensState();
}

class _ScreensState extends State<Screens> {
  int _selectedIndex = 0;
  static List<Widget> _widgetOptions = <Widget>[
    HomeScreen(),
    CartScreen(),
    Container(),
    Container(),
  ];

  @override
  Widget build(BuildContext context) {
     return Scaffold(
      backgroundColor: Colors.white,
      body: _widgetOptions.elementAt(_selectedIndex),
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              blurRadius: 20,
              color: Colors.black.withOpacity(0.1),
            ),
          ],
        ),
        child: SafeArea(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 15,vertical: 8),
            child: GNav(
              iconSize: 25,
              tabBackgroundColor: Colors.redAccent,
              activeColor: Colors.white,
              gap: 8,
              padding: EdgeInsets.symmetric(horizontal: 20,vertical: 13),
              tabs:[
                GButton(icon:LineIcons.home, text: 'Home'),
                GButton(icon:LineIcons.shoppingBag, text: 'Cart'),
                GButton(icon:LineIcons.heart, text: 'Wishlist'),
                GButton(icon:LineIcons.user, text: 'Account'),
              ],
              selectedIndex: _selectedIndex,
              onTabChange: (index){
                setState(() {
                  _selectedIndex = index;
                });
              } ,
              ),
          ),
        ),
      ),
      );
   
  }
}
