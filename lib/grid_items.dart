import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:screen_design/Api/load_local_json.dart';
import 'package:screen_design/item_screen.dart';

class GridItems extends StatelessWidget {
  var aName = ["watch", "Tshirt", "Nike-Shoes", "Ear Headphone"];

  var pName = ["Apple Watch -M2", "White Tshirt", "Nike Shoe", "Ear Headphone"];

  @override
  Widget build(BuildContext context) {
    List<String> name = [
      'Apple Watch -M2',
      'White Tshirt',
      'Nike Shoes',
      'Ear Headphone'
    ];
    List<Map<String, dynamic>> imgpath = [
      {
        'img1': "images/watch.png",
        'img2': "images/watch2.png",
        'img3': "images/watch.png"
      },
      {
        'img1': "images/Tshirt.png",
        'img2': "images/Tshirt2.png",
        'img3': "images/Tshirt3.png"
      },
      {
        'img1': "images/Nike-Shoes.png",
        'img2': "images/Nike-Shoes2.png",
        'img3': "images/Nike-Shoes3.png"
      },
      {
        'img1': "images/Ear Headphone.png",
        'img2': "images/Ear Headphone2.png",
        'img3': "images/Ear Headphone3.png"
      },
      {
        'img1': "images/Mi-phone.png",
        'img2': "images/Mi-PNG-Isolated-Pic.png",
        'img3': "images/Mi-PNG-Photo2.png"
      },
      {
        'img1': "images/Portable-Laptop.png",
        'img2': "images/Portable-Laptop2.png",
        'img3': "images/Laptop3.png"
      }
    ];

    return FutureBuilder<dynamic>(builder: (context, snapshot) {
      if (snapshot.hasData) {
        // List<dynamic> datas = jsonDecode(snapshot.data!.body.toString());
        // datas.reversed;
        return GridView.builder(
          itemCount: 6,
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            childAspectRatio: 0.7,
            crossAxisCount: 2,
          ),
          itemBuilder: (context, index) {
            return InkWell(
              onTap: () {

              },
              child: Container(
                margin: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Color(0xFFD4ECF7),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black26,
                      blurRadius: 4,
                      spreadRadius: 2,
                    ),
                  ],
                ),
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "30% off",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 14,
                            ),
                          ),
                          Icon(
                            Icons.favorite,
                            color: Colors.red,
                          ),

                        ],
                      ),
                      SizedBox(height: 10),
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => ItemScreen(
                                  img1: imgpath[index]['img1'],
                                  img2: imgpath[index]['img2'],
                                  img3: imgpath[index]['img3'],
                                  name: snapshot.data![index]['name'].toString(),
                                  price: snapshot.data![index]['price'].toString(),
                                  price2: snapshot.data![index]['price1'].toString(),
                                ),
                              ),
                            );
                          },
                          child: Image.network(
                            snapshot.data![index]['avatar'].toString(),
                            height: 100,
                            width: 100,
                          ),
                        ),
                      ),
                      SizedBox(height: 15),
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              snapshot.data[index]['name'].toString(),
                              style: TextStyle(
                                fontSize: 17,
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(height: 10),
                            Row(
                              children: [
                                Text(
                                  snapshot.data[index]['price'].toString(),
                                  style: TextStyle(
                                    fontSize: 16,
                                    color: Colors.red,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                                SizedBox(width: 5),
                                Text(
                                  snapshot.data[index]['price1'].toString(),
                                  style: TextStyle(
                                    decoration: TextDecoration.lineThrough,
                                    color: Colors.black.withOpacity(0.5),
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          },
        );
      } else {
        return Center(child: CircularProgressIndicator());
      }
    },future: getDataFromWebServer());
  }
}
